package org.eapo.simpleservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleserviseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleserviseApplication.class, args);
	}
}
