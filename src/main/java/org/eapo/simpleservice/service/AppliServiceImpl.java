package org.eapo.simpleservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AppliServiceImpl implements AppliService {

    private final static String SQL = "select idappli, title from ptappli limit 10";

    @Autowired
    JdbcTemplate template;

    @Override

    public List getAll() {
        return template.queryForList(SQL);
    }

}
