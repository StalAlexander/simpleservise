package org.eapo.simpleservice.rest;


import org.eapo.simpleservice.service.AppliService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AppliController {

    @Autowired
    private AppliService service;


    @RequestMapping("/")
    public String root() {
        return "user rest service";
    }

    @RequestMapping("/all")
    public List all() {
        return service.getAll();
    }


}
